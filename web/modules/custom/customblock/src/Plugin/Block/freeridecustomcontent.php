<?php

namespace Drupal\freeridecustomcontent\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'freeridecustomcontent' Block.
 *
 * @Block(
 *   id = "freeridecustomcontent_Block",
 *   admin_label = @Translation("freeridecustomcontent Block"),
 *   category = @Translation("freeridecustomcontent Block"),
 * )
 */
class freeridecustomcontent extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build= array(
         	'#theme' => 'freeridecustomcontent_block',
         	 '#firstvar' => 'sravan',
         	  '#secondvar' => 24,
      // '#type' => 'markup',
      // '#markup' => '<h2>Hello</h2>',
    	);
    return $build;
  }

}