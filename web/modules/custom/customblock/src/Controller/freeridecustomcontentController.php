<?php

namespace Drupal\freeridecustomcontent\Controller;

use Drupal\Core\Controller\ControllerBase;

class freeridecustomcontentController extends ControllerBase {

  /**
   * Display the markup.
   *
   * @return array
   */
  public function content() {
    return array(
      '#type' => 'markup',
      '#markup' => $this->t('Welcome to custom controller!'),
    );
  }

}