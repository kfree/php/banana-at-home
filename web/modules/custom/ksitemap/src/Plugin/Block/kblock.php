<?php

namespace Drupal\ksitemap\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'kblock' Block.
 *
 * @Block(
 *   id = "kblock",
 *   admin_label = @Translation("kblock Block"),
 *   subject = @Translation("kblock Block"),
 * )
 */
class kblock extends BlockBase
{

    /**
     * {@inheritdoc}
     */
    public function build()
    {
        $build = array(
            '#type' => 'markup',
            '#markup' => '<h1>Hello</h1>',
        );
        return $build;
    }

}